<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8" />
<title>The Panchatantra, in Sanskrit and English</title>
<link rel="stylesheet" href="navstyle.css" type="text/css" />
<link rel="stylesheet" href="mystyle.css" type="text/css" />
</head>
<body>
<nav>
<ul class="nav">
  <li><a href="index.html">Home</a></li>
  <li><a class="active" href="ryder.html">Ryder's introduction</a></li>
  <li><a href="lanman.html">Lanman's notes</a></li>
  <li><a class="separator">&nbsp;</a></li>
  <li><a href="introduction.html">Introduction (kathāmukham)</a></li>
  <li><a href="book1.html">Book I</a></li>
  <li><a href="book2.html">Book II</a></li>
  <li><a href="book3.html">Book III</a></li>  
  <li><a href="book4.html">Book IV</a></li>
  <li><a href="book5.html">Book V</a></li>
</ul>
</nav>

(The translator, Arthur W. Ryder, wrote a wonderful introduction to the book. That is included below.)

  <div id="TOC">
        <ul>
          <li>
            <a href="#translators-introduction">Translator’s
            Introduction</a>

            <ul>
              <li><a href="#i">I</a></li>

              <li><a href="#ii">II</a></li>

              <li><a href="#iii">III</a></li>
            </ul>
          </li>
        </ul>
  </div>

<main>
  <div id="introductions">
    <p class="parbreak"><br /></p>

    <div id="translators-introduction">
      <h2><a href="#TOC">Translator’s Introduction</a></h2>

      <p class="parbreak"><br /></p>

      <div id="i">
        <h3><a href="#TOC">I</a></h3>
        <div class="pagenum">सकलार्थशास्त्रसारं / जगति समालोक्य विष्णुशर्मेदम्। <br/>
                तन्त्रैः पञ्चभिर् एतच् / चकार सुमनोहरं शास्त्रम् ॥ (Kathāmukham 1)</div>
        <pre>
One Vishnusharman, shrewdly gleaning
All worldly wisdom's inner meaning,
In these five books the charm compresses
Of all such books the world possesses.
        &mdash;Introduction to the <i>Panchatantra</i>
</pre>

        <p class="parbreak"><br /></p>

        <p>The <em>Panchatantra</em> contains the most widely known
        stories in the world. If it were further declared that the
        <em>Panchatantra</em> is the best collection of stories in
        the world, the assertion could hardly be disproved, and
        would probably command the assent of those possessing the
        knowledge for a judgment. Assuming varied forms in their
        native India, then traveling in translations, and
        translations of translations, through Persia, Arabia,
        Syria, and the civilized countries of Europe, these stories
        have, for more than twenty centuries, brought delight to
        hundreds of millions.</p>

        <p class="parbreak"><br /></p>

        <p>Since the stories gathered in the <em>Panchatantra</em>
        are very ancient, and since they can no longer be ascribed
        to their respective authors, it is not possible to give an
        accurate report of their genesis, while much in their
        subsequent history will always remain obscure.
        Dr.&nbsp;Hertel, the learned and painstaking editor of the
        text used by the present translator, believes that the
        original work was composed in Kashmir, about 200 B.C. At
        this date, however, many of the individual stories were
        already ancient. He then enumerates no less than
        twenty-five recensions of the work in India. The text here
        translated is late, dating from the year 1199 A.D.</p>

        <p class="parbreak"><br /></p>

        <p>It is not here intended to summarize the history of
        these stories in India, nor their travels through the Near
        East and through Europe. The story is attractive—whose
        interest is not awakened by learning, for example, that in
        this work he makes the acquaintance of one of La Fontaine’s
        important sources? Yet here, as elsewhere, the work of the
        “scholars” has been of somewhat doubtful value, diverting
        attention from the primary to the secondary, from
        literature itself to facts, more or less important, about
        literature. The present version has not been made by a
        scholar, but by the opposite of a scholar, a lover of good
        books, eager, so far as his powers permit, to extend an
        accurate and joyful acquaintance with the world’s
        masterpieces. He will therefore not endeavor to tell the
        history of the <em>Panchatantra</em>, but to tell what the
        <em>Panchatantra</em> is.</p>

        <p class="parbreak"><br /></p>
      </div>

      <div id="ii">
        <h3><a href="#TOC">II</a></h3>
        <div class="pagenum">यो चैतत् पठति प्रायो / नीतिशास्त्रं शृणोति वा। <br/>
                न पराभवम् आप्नोति / स शक्राद् अपि कर्हिचित् ॥ (Kathāmukham 5)</div>
        <pre>
Whoever learns the work by heart,
Or through the story-teller's art
    Becomes acquainted,
His life by sad defeat&mdash;although
The king of heaven be his foe&mdash;
    Is never tainted.
        &mdash;Introduction to the <i>Panchatantra</i>
</pre>

        <p class="parbreak"><br /></p>

        <p class="parbreak"><br /></p>

        <p>The <em>Panchatantra</em> is a <em>niti-shastra</em>, or
        textbook of <em>niti</em>. The word <em>niti</em> means
        roughly “the wise conduct of life.” Western civilization
        must endure a certain shame in realizing that no precise
        equivalent of the term is found in English, French, Latin,
        or Greek. Many words are therefore necessary to explain
        what <em>niti</em> is, though the idea, once grasped, is
        clear, important, and satisfying.</p>

        <p class="parbreak"><br /></p>

        <p>First of all, <em>niti</em> presupposes that one has
        considered, and rejected, the possibility of living as a
        saint. It can be practiced only by a social being, and
        represents an admirable attempt to answer the insistent
        question how to win the utmost possible joy from life in
        the world of men.</p>

        <p class="parbreak"><br /></p>

        <p>The negative foundation is security. For example, if one
        is a mouse, his dwelling must contain recesses beyond the
        reach of a cat’s paw. Pleasant stanzas concerning the
        necessity of security are scattered throughout the work.
        Thus:</p>
        <div class="pagenum">प्रच्छन्नं किल भोक्तव्यं / दरिद्रेण विशेषतः । <br>
                पश्य भोजनदौर्बल्याद् / धुडः केसरिणा हतः ॥  (I.344)</div>
        <pre>
The poor are in peculiar need
Of being secret when they feed;
The lion killed the ram who could
Not check his appetite for food.
</pre>

        <p>or again:</p>
        <div class="pagenum">निःसर्पे बद्धसर्पे वा / भवने सुप्यते सुखम् । <br> 
                दृष्टनष्टभुजंगे तु / निद्रा दुःखेन लभ्यते ॥ (III.226)</div>
        <pre>
In houses where no snakes are found,
One sleeps; or where the snakes are bound:
But perfect rest is hard to win
With serpents bobbing out and in.
</pre>

        <p>The mere negative foundation of security requires a
        considerable exercise of intelligence, since the world
        swarms with rascals, and no sensible man can imagine them
        capable of reformation.</p>
        <div class="pagenum">दुर्जनः प्रकृतिं याति / सेव्यमानो ऽपि यत्नतः । <br>
                स्वेदनाभ्यञ्जनोपायैः / श्वपुच्छम् इव नामितम् ॥ (I.240)</div>
        <pre>
Caress a rascal as you will,
He was and is a rascal still:
All salve- and sweating-treatments fail
To take the kink from doggy's tail.
</pre>

        <p>Yet roguery can be defeated; for by its nature it is
        stupid.</p>
        <div class="pagenum">सर्पाणां दुर्जनानां च  /  परच्छिद्रानुजीविनाम् । <br>
                अभिप्राया न सिध्यन्ति  /  तेनेदं वर्तते जगत्  ॥ (I.327)</div>
        <pre>
Since scamp and sneak and snake
So often undertake
A plan that does not thrive,
The world wags on, alive.
</pre>

        <p class="parbreak"><br /></p>

        <p>Having made provision for security, in the realization
        that</p>
        <div class="pagenum">जीवन नरो भद्रशतानि पश्यति ॥ (IV)</div>
        <pre>
A man to thrive
Must keep alive,
</pre>

        <p>one faces the necessity of having money. The
        <em>Panchatantra</em>, being very wise, never falls into
        the vulgar error of supposing money to be important. Money
        must be there, in reasonable amount, because it is
        unimportant, and what wise man permits things unimportant
        to occupy his mind? Time and again the
        <em>Panchatantra</em> insists on the misery of poverty,
        with greatest detail in the story of “Gold’s Gloom” in the
        second book, never perhaps with more point than in the
        stanza:</p>
        <div class="pagenum">उत्तिष्ठ क्षणम् एकम् उद्वह सखे दारिद्यभारं मम <br>
                श्रान्तस् तावद् अहं चिरान् मरणजं सेवे त्वदीयं सुखम् । <br>
                इत्य् उक्तो धनवर्जितेन सहसा गत्वा श्मशाने शवो <br>
                दारिद्र्यान् मरणं परं सुखम् इति ज्ञात्वेव तूष्णीं स्थितः ॥ (V.18)</div>
        <pre>
A beggar to the graveyard hied
And there “Friend corpse, arise,” he cried;
“One moment lift my heavy weight
Of poverty; for I of late
Grow weary, and desire instead
Your comfort; you are good and dead.”
The corpse was silent. He was sure
'Twas better to be dead than poor.
</pre>

        <p class="parbreak"><br /></p>

        <p>Needless to say, worldly property need not be, indeed
        should not be, too extensive, since it has no value in
        possession, but only in use:</p>
        <div class="pagenum">अश्वः शस्त्रं शास्त्रं / वीणा वाणी नरश् च नारी च । <br>
                पुरुषविशेषं प्राप्ता  / भवन्त्य् अयोग्याश् च योग्याश् च ॥ (I.69 = I.84)</div>
        <pre>
In case of horse or book or sword,
Of woman, man or lute or word,
The use or uselessness depends
On qualities the user lends.
</pre>

        <p class="parbreak"><br /></p>

        <p>Now for the positive content of <em>niti</em>. Granted
        security and freedom from degrading worry, then joy results
        from three occupations—from resolute, yet circumspect, use
        of the active powers; from intercourse with like-minded
        friends; and above all, from worthy exercise of the
        intelligence.</p>

        <p class="parbreak"><br /></p>

        <p>Necessary, to begin with, for the experience of true joy
        in the world of men, is resolute action. The difficulties
        are not blinked:</p>
        <div class="pagenum">क्लेशस्याङ्गम् अदत्वा / सुखम् एव सुखानि नेह लभ्यन्ते । (V.31)</div>
        <pre>
There is no toy
Called easy joy;
But man must strain
To body's pain.
</pre>

        <p>Time and again this note is struck—the difficulty and
        the inestimable reward of sturdy action. Perhaps the most
        splendid expression of this essential part of <em>niti</em>
        is found in the third book, in the words which the crow,
        Live-Strong, addresses to his king, Cloudy</p>
        <div class="pagenum">विस्तीर्णव्यवसायसाध्यमहतां स्निग्धैः प्रयुक्ताशिषां <br>
                कार्याणां नयसाहसोन्नतिमताम् इच्छापदारोहिणाम् । <br>
                मानोत्सेकपराक्रमव्यसनिनः पारं न यावद् गताः <br>
                सामर्षे हृदये ऽवकाशविषया तावत् कथं निर्वृतिः ॥ (III.227)</div>
        <pre>
A noble purpose to attain
Desiderates extended pain,
Asks man's full greatness, pluck, and care,
And loved ones aiding with a prayer.
Yet if it climb to heart's desire,
What man of pride and fighting fire,
Of passion and of self-esteem
Can bear the unaccomplished dream?
His heart indignantly is bent
(Through its achievement) on content.
</pre>

        <p class="parbreak"><br /></p>

        <p>Equal stress is laid upon the winning and holding of
        intelligent friends. The very name of the second book is
        “The Winning of Friends”; the name of the first book is
        “The Loss of Friends.” Throughout the whole work, we are
        never permitted to be long oblivious of the rarity, the
        necessity, and the pricelessness of friendship with the
        excellent. For, indeed,</p>
        <div class="pagenum">अविरलम् अप्य् अनुभूताः / शिष्टेष्टसमागमेषु ये दिवसाः । <br>
                पथ्यटनसंनिभास् ते / जीवितकान्तारशेषस्य ॥ (II.180)</div>
        <pre>
The days when meetings do not fail
    With wise and good
Are lovely clearings on the trail
    Through life's wild wood.
</pre>

        <p>So speaks Slow, the turtle; and Swift, the crow,
        expresses it thus:</p>
        <div class="pagenum">सुखस्य सारः परिभुज्यते तैर्  <br>
                जीवन्ति ते सत्पुरुषास् त एव । <br>
                हृष्टाः सुहृष्टैः सुहृदः सुहृद्भिः <br>
                प्रियाः प्रियैर् ये सहिता रमन्ते ॥ (II.163)
        </div>
        <pre>
They taste the best of bliss, are good,
    And find life's truest ends,
Who, glad and gladdening, rejoice
    In love, with loving friends.
</pre>

        <p class="parbreak"><br /></p>

        <p>Last of all, and in a sense including all else, is the
        use of the intelligence. Without it, no human joy is
        possible, nothing beyond animal happiness.</p>
        <div class="pagenum">अहितहितविचारशून्यबुद्धेः <br>
                श्रुतिसमयैर् बहुभिर् बहिष्कृतस्य । <br>
                उदरभरणमात्रम् एव लिप्सोः <br>
                पुरुषपशोश् च पशोश् च को विशेषः ॥ (I.15)
        </div>
        <pre>
For if there be no mind
    Debating good and ill,
And if religion send
    No challenge to the will,
If only greed be there
    For some material feast,
How draw a line between
    The man-beast and the beast?
</pre>

        <p>One must have at disposal all valid results of
        scholarship, yet one must not be a scholar. For</p>
        <div class="pagenum">वरं बुद्धिर् न सा विद्या / विद्यातो बुद्धिर् उत्तमा । (V.33)</div>
        <pre>
Scholarship is less than sense;
Therefore seek intelligence.
</pre>

        <p>One must command a wealth of detailed fact, ever alert
        to the deceptiveness of seeming fact, since oftentimes</p>
        <div class="pagenum">तलवद् दृश्यते व्योम / खद्योतो हव्यवाड् इव । <br>
                न तलं विद्यते व्योम्नि / न खद्योतो हुताशनः ॥ (I.438)</div>
        <pre>
The firefly seems a fire, the sky looks flat;
Yet sky and fly are neither this nor that.
</pre>

        <p>One must understand that there is no substitute for
        judgment, and no end to the reward of discriminating
        judgment:</p>
        <div class="pagenum">दुःखम् आत्मा परिच्छेत्तुम् / इति योग्यो न वेत्ति वा । <br>
                इदं यस्यास्ति विज्ञानं  /  न स कृच्छ्रेषु सीदति ॥ (I.323)</div>
        <pre>
To know oneself is hard, to know
    Wise effort, effort vain;
But accurate self-critics are
    Secure in times of strain.
</pre>

        <p>One must be ever conscious of the past, yet only as it
        offers material for wisdom, never as an object of brooding
        regret:</p>
        <div class="pagenum">नष्टं मृतम् अतिक्रान्तं  /  नानुशोचन्ति पण्डिताः  । <br>
                पण्डितानां च मूर्खाणां  /  विशेषो ऽयं यतः स्मृतः ॥ (I.336)</div>
        <pre>
For lost and dead and past
    The wise have no laments:
Between the wise and fools
    Is just this difference.
</pre>

        <p>This is the lofty consolation offered by a wood-pecker
        to a hen-sparrow whose eggs have been crushed by an
        elephant with the spring fever. And the whole matter finds
        its most admirable expression in the noble words of Cheek,
        the jackal:</p>
        <div class="pagenum">यां लब्ध्वेन्द्रियनिग्रहो न महता भावेन संपद्यते <br>
                या बुद्धेर् न विधेयतां प्रकुरुते धर्मे न या वर्तते । <br>
                लोके केवलवाक्यमाचरचना यां प्राप्य संजायते <br>
                या नैवोपशमाय नापि यशसे विद्वत्तया किं तया ॥ (I.365)</div>
        <pre>
What is learning whose attaining
Sees no passion wane, no reigning
    Love and self-control?
Does not make the mind a menial,
Finds in virtue no congenial
    Path and final goal?
Whose attaining is but straining
For a name, and never gaining
    Fame or peace of soul?
</pre>

        <p class="parbreak"><br /></p>

        <p>This is <em>niti</em>, the harmonious development of the
        powers of man, a life in which security, prosperity,
        resolute action, friendship, and good learning are so
        combined as to produce joy. It is a noble ideal, shaming
        many tawdry ambitions, many vulgar catchwords of our day.
        And this noble ideal is presented in an artistic form of
        perfect fitness, in five books of wise and witty stories,
        in most of which the actors are animals.</p>

        <p class="parbreak"><br /></p>
      </div>

      <div id="iii">
        <h3><a href="#TOC">III</a></h3>
        <div class="pagenum">वरं नरकवासो ऽपि / विद्वद्भिः सहितो मम । <br>
                न नीचजनसंपर्कः / सुरेन्द्रभुवनेष्व् अपि ॥ (II.168)</div>
        <pre>
Better with the learned dwell,
Even though it be in hell
Than with vulgar spirits roam
Palaces that gods call home.
        &mdash;<i>Panchatantra</i>, Book II
</pre>

        <p class="parbreak"><br /></p>

        <p>The word <em>Panchatantra</em> means the “Five Books,”
        the Pentateuch. Each of the five books is independent,
        consisting of a framing story with numerous inserted
        stories, told, as fit circumstances arise, by one or
        another of the characters in the main narrative. Thus, the
        first book relates the broken friendship of the lion Rusty
        and the bull Lively, with some thirty inserted stories,
        told for the most part by the two jackals, Victor and
        Cheek. The second book has as its framing story the tale of
        the friendship of the crow, the mouse, the turtle, and the
        deer, whose names are Swift, Gold, Slow, and Spot. The
        third book has as framing story the war between crows and
        owls.</p>

        <p class="parbreak"><br /></p>

        <p>These three books are of considerable length and show
        great skill in construction. A somewhat different
        impression is left by Books IV and V. The framing story of
        Book IV, the tale of the monkey and the crocodile, has less
        interest than the inserted stories, while Book V can hardly
        be said to have a framing story, and it ends with a couple
        of grotesque tales, somewhat different in character from
        the others. These two shorter books, in spite of the charm
        of their contents, have the appearance of being addenda,
        and in some of the older recensions are reduced in bulk to
        the verge of extinction.</p>

        <p class="parbreak"><br /></p>

        <p>The device of the framing story is familiar in oriental
        works, the instance best known to Europeans being that of
        the <em>Arabian Nights</em>. Equally characteristic is the
        use of epigrammatic verses by the actors in the various
        tales. These verses are for the most part quoted from
        sacred writings or other sources of dignity and authority.
        It is as if the animals in some English beast-fable were to
        justify their actions by quotations from Shakespeare and
        the Bible. These wise verses it is which make the real
        character of the <em>Panchatantra</em>. The stories,
        indeed, are charming when regarded as pure narrative; but
        it is the beauty, wisdom, and wit of the verses which lift
        the <em>Panchatantra</em> far above the level of the best
        story-books. It hardly needs to be added that in the
        present version, verse is always rendered by verse, prose
        by prose. The titles of the individual stories, however,
        have been supplied by the translator, since the original
        has none.</p>

        <p class="parbreak"><br /></p>

        <p>The large majority of the actors are animals, who have,
        of course, a fairly constant character. Thus, the lion is
        strong but dull of wit, the jackal crafty, the heron
        stupid, the cat a hypocrite. The animal actors present, far
        more vividly and more urbanely than men could do, the view
        of life here recommended—a view shrewd, undeceived, and
        free of all sentimentality; a view that, piercing the
        humbug of every false ideal, reveals with incomparable wit
        the sources of lasting joy.</p>
        <pre>
                            Arthur W. Ryder
                            Berkeley, California
                            July, 1925
</pre>

        <p class="parbreak"><br /></p><!-- Break -->

        <p class="parbreak"><br /></p>
      </div>
    </div>


      <p class="parbreak"><br /></p>
    </div>
  </div>
</main>
</body>
</html>
