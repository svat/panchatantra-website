Notes on the images. All are of Volume 11 of HOS.

-   `hertel/` (85 MiB + 90 MiB from below)

    - Contains `.png` images from Google Books, cropped. (White background, low resolution.) `00000062.png` is the first.

-   `hertel/resized/` (90 MiB)

    - Contains `.png` images of the originals, without trimming; `00000062.png` is the first.

-   `hertel-11/orig/panchatantracoll00purn_orig_jp2/` (256 MiB)

    - Contains `.jp2` images of full pages (with scaffolding; need rotation!); `panchatantracoll00purn_orig_0059.jp2` is the first.

-   `hertel-11/non-orig/panchatantracoll00purn_jp2/` (133 MiB)

    - Contains `.jp2` images of full pages (with proper rotation and cropping); `panchatantracoll00purn_0059.jp2` is the first.

-   `hertel-stashed/` (1733 MiB)

    - Contains `.png` images of full pages (probably converted from the above); `00000062.png` is the first.
